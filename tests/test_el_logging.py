# -*- coding: utf-8 -*-

import unittest

from el_logging import logger


class TestElLogging(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.info("Starting 'el_logging' unittest...\n")

    @classmethod
    def tearDownClass(cls):
        logger.success("Successfully tested 'el_logging'.")


    def test_init(self):
        logger.info("Testing initialization of 'el_logging' module...")
        self.assertIsNotNone(logger)
        logger.success('Done.\n')

    def test_functions(self):
        logger.info("Testing basic functions of 'el_logging'...")
        logger.trace('Tracing...')
        logger.debug('Debugging...')
        logger.info('Logging info.')
        logger.success('Success.')
        logger.warning('Warning something.')
        logger.error('Error occured.')
        logger.critical('CRITICAL ERROR.')
        self.assertTrue(True)
        logger.success("Done.\n")
